import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		menuToggle: false,
		menuCategory: false,
		modalSocials: [
			{
				url: '#',
				srcImg: require('@/assets/images/fb.svg')
			},
			{
				url: '#',
				srcImg: require('@/assets/images/tw.svg')
			},
			{
				url: '#',
				srcImg: require('@/assets/images/g-plus.svg')
			},
			{
				url: '#',
				srcImg: require('@/assets/images/LinkedIn.svg')
			}
		],
	},
	mutations: {
	},
	actions: {
	},
	modules: {
	}
})
