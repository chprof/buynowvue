import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Personal from '../views/Personal.vue'
import myShop from '../views/myShop.vue'
import shopSettings from '../views/ShopSettings.vue'
import shopInside from '../views/ShopInside.vue'
import shopEvents from '../views/shopEvents.vue'
import shopCoupons from '../views/shopCoupons.vue'
import shopReviews from '../views/shopReviews.vue'
import shopQuiz from '../views/shopQuiz.vue'
import createGood from '../views/createGood.vue'
import myLots from '../views/myLots.vue'
import createLot from '../views/createLot.vue'
import myBasket from '../views/myBasket.vue'
import myBuynow from '../views/my-buynow.vue'
import store from '../views/Store.vue'
import storeEvents from '../views/storeEvents.vue'
import storeCoupons from '../views/storeCoupons.vue'
import storeReviews from '../views/storeReviews.vue'
import storeQuiz from '../views/storeQuiz.vue'
import advertPayment from '../views/advertPayment.vue'
import goodAdded from '../views/good-added.vue'
import categorys from '../views/categorys-view.vue'
import electronics from '../views/electronics.vue'
import clocks from '../views/clocks.vue'
import goodPage from '../views/good-page.vue'
import goodPageAdvert from '../views/good-page-advert.vue'
import goodPageSeller from '../views/good-page-seller.vue'
import filter from '../views/filters.vue'
import messeges from '../views/Messeges.vue'
import support from '../views/support.vue'
import addCategory from '../views/addCategory.vue'
import lotInside from '../views/lot-inside.vue'
import myWish from '../views/my-wish.vue'
import browsingHistory from '../views/browsing-history.vue'
import myOrders from '../views/my-orders.vue'
import myCoupons from '../views/my-coupons.vue'
import myPolls from '../views/my-polls.vue'
import articleCat from '../views/article-category.vue'
import articleCard from '../views/article-card.vue'
import referals from '../views/referals.vue'
import createQuiz from '../views/create-quiz.vue'
import myAds from '../views/my-ads.vue'
import createAdSell from '../views/create-ad-sell.vue'
import createAdBuy from '../views/create-ad-buy.vue'
import adInside from '../views/ad-inside.vue'
// import AGuide from '../views/Guide.vue'

Vue.use(VueRouter)

const routes = [
	{
		path: '/',
		name: 'home',
		component: Home,
		meta: {
			breadcrumb: 'Главная'
		},
		children: [
			{
				path: 'my-buynow',
				name: 'my-buynow',
				component: myBuynow,
				meta: {
					breadcrumb: 'Мой Buynow'
				},
				children: [
					{
						path: 'personal',
						name: 'personal',
						component: Personal,
						meta: {
							breadcrumb: 'Мой профиль'
						},
					},
					{
						path: 'my-basket',
						name: 'my-basket',
						component: myBasket,
						meta: {
							breadcrumb: 'Корзина'
						},
					},
					{
						path: 'messeges',
						name: 'messeges',
						component: messeges,
						meta: {
							breadcrumb: 'Центр сообщений'
						},
					},
					{
						path: 'support',
						name: 'support',
						component: support,
						meta: {
							breadcrumb: 'Центр поддержки'
						},
					},
					{
						path: 'my-wish',
						name: 'my-wish',
						component: myWish,
						meta: {
							breadcrumb: 'Мои желания'
						},
					},
					{
						path: 'browsing-history',
						name: 'browsing-history',
						component: browsingHistory,
						meta: {
							breadcrumb: 'История просмотров'
						},
					},
					{
						path: 'my-orders',
						name: 'my-orders',
						component: myOrders,
						meta: {
							breadcrumb: 'Мои заказы'
						},
					},
					{
						path: 'my-coupons',
						name: 'my-coupons',
						component: myCoupons,
						meta: {
							breadcrumb: 'Мои купоны'
						},
					},
					{
						path: 'my-polls',
						name: 'my-polls',
						component: myPolls,
						meta: {
							breadcrumb: 'Мои опросы'
						},
					},
					{
						path: 'referals',
						name: 'referals',
						component: referals,
						meta: {
							breadcrumb: 'Рефералы'
						},
					},
					{
						path: 'article-category',
						name: 'article-category',
						component: articleCat,
						meta: {
							breadcrumb: 'База знаний'
						},
						children: [
							{
								path: 'article-card',
								name: 'article-card',
								component: articleCard,
								meta: {
									breadcrumb: 'Статья'
								},
							},
						]
					},
					{
						path: 'my-shop',
						name: 'my-shop',
						component: myShop,
						meta: {
							breadcrumb: 'Мой магазин'
						},
						children: [
							{
								path: 'shop-inside',
								name: 'shop-inside',
								component: shopInside,
								meta: {
									breadcrumb: 'Магазин'
								},
								children: [
									{
										path: 'shop-events',
										name: 'shop-events',
										component: shopEvents,
										meta: {
											breadcrumb: 'Акции'
										},
									},
									{
										path: 'shop-reviews',
										name: 'shop-reviews',
										component: shopReviews,
										meta: {
											breadcrumb: 'Отзывы'
										},
									},
									{
										path: 'shop-quiz',
										name: 'shop-quiz',
										component: shopQuiz,
										meta: {
											breadcrumb: 'Опросы'
										},
									},
									{
										path: 'shop-coupons',
										name: 'shop-coupons',
										component: shopCoupons,
										meta: {
											breadcrumb: 'Купоны'
										},
									},
									{
										path: 'shop-create-good',
										name: 'shop-create-good',
										component: createGood,
										meta: {
											breadcrumb: 'Добавить товар'
										},
									},
									{
										path: 'shop-create-quiz',
										name: 'shop-create-quiz',
										component: createQuiz,
										meta: {
											breadcrumb: 'Создать опрос'
										},
									},
								]
							},
							{
								path: 'shop-settings',
								name: 'shop-settings',
								component: shopSettings,
								meta: {
									breadcrumb: 'Настройки магазина'
								},
							}
						]
					},
					{
						path: 'my-lots',
						name: 'my-lots',
						component: myLots,
						meta: {
							breadcrumb: 'Мои лоты'
						},
						children: [
							{
								path: 'create-lot',
								name: 'create-lot',
								component: createLot,
								meta: {
									breadcrumb: 'Создание лота'
								},
							},
							{
								path: 'lot-inside',
								name: 'lot-inside',
								component: lotInside,
								meta: {
									breadcrumb: 'Лот'
								},
							},
						]
					},
					{
						path: 'my-ads',
						name: 'my-ads',
						component: myAds,
						meta: {
							breadcrumb: 'Мои объявления'
						},
						children: [
							{
								path: 'create-ad-sell',
								name: 'create-ad-sell',
								component: createAdSell,
								meta: {
									breadcrumb: 'Создание объявления на продажу'
								},
							},
							{
								path: 'create-ad-buy',
								name: 'create-ad-buy',
								component: createAdBuy,
								meta: {
									breadcrumb: 'Создание объявления на покупку'
								},
							},
							{
								path: 'ad-inside',
								name: 'ad-inside',
								component: adInside,
								meta: {
									breadcrumb: 'Просмотр объявления'
								},
							},
						]
					},
				]
			},
			{
				path: 'store-inside',
				name: 'store-inside',
				component: store,
				meta: {
					breadcrumb: 'Store inside'
				},
				children: [
					{
						path: 'store-events',
						name: 'store-events',
						component: storeEvents,
						meta: {
							breadcrumb: 'Акции'
						},
					},
					{
						path: 'store-reviews',
						name: 'store-reviews',
						component: storeReviews,
						meta: {
							breadcrumb: 'Отзывы'
						},
					},
					{
						path: 'store-quiz',
						name: 'store-quiz',
						component: storeQuiz,
						meta: {
							breadcrumb: 'Опросы'
						},
					},
					{
						path: 'store-coupons',
						name: 'store-coupons',
						component: storeCoupons,
						meta: {
							breadcrumb: 'Купоны'
						},
					},
				]
			},
			{
				path: 'categorys',
				name: 'categorys',
				component: categorys,
				meta: {
					breadcrumb: 'Категории'
				},
				children: [
					{
						path: 'electronics',
						name: 'electronics',
						component: electronics,
						meta: {
							breadcrumb: 'Электроника',
							testRoute: true
						},
						children: [
							{
								path: 'clocks',
								name: 'clocks',
								component: clocks,
								meta: {
									breadcrumb: 'Часы'
								},
								children: [
									{
										path: 'good-page',
										name: 'good-page',
										component: goodPage,
										meta: {
											breadcrumb: 'name of good'
										}
									},
									{
										path: 'good-page-advert',
										name: 'good-page-advert',
										component: goodPageAdvert,
										meta: {
											breadcrumb: 'name of advert'
										}
									},
									{
										path: 'good-page-seller',
										name: 'good-page-seller',
										component: goodPageSeller,
										meta: {
											breadcrumb: 'name of seller'
										}
									}
								]
							}
						]
					},
				]
			},
			{
				path: 'filter',
				name: 'filter',
				component: filter
			},
			{
				path: 'add-category',
				name: 'add-category',
				component: addCategory
			}
		]
	},
	{
		path: '/advert-payment',
		name: 'advert-payment',
		component: advertPayment,
		meta: {
			breadcrumb: 'Оплата'
		},
	},
	{
		path: '/good-added',
		name: 'good-added',
		component: goodAdded,
		meta: {
			breadcrumb: 'Товар добавлен'
		},
	}
]

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	// base: '/',
	routes
})

export default router
